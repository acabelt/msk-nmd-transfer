<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">


<link rel="shortcut icon" href="/favicon.ico">
<link rel="icon" type="image/png" href="/favicon.png" sizes="64x64">


<title><?php echo $title; // aus der _init.php ?></title>
<meta name="robots" content="noindex,nofollow">
<meta name="description" content="">
<meta property="og:image" content="">
<style>
    body {font-family:Verdana, sans-serif;}
    code {background:#ddd; border-radius:3px; color:#c00; padding:2px 3px;}
    td {font-size:12px; padding:2px 4px;}
</style>
</head>
<body class=" ">
<?php
    
    $hier = "1028";
    $hier2 = "1045";
    $hier3 = "1170";
    
    $ueberall = $pages->find("has_parent=$hier3");
?>
<table border="1">
<tr>
    <th>ID</th>
    <th>Marke</th>
    <th>Name</th>
    <th>1</th>
    <th>2</th>
    <th>3</th>
    <th>Preis</th>
</tr>
<?php foreach($pages->find("template=product, sort=id") as $p): ?>
<tr>
    <td><?php echo $p->id; ?></td>
    <td><?php echo $p->vendor->title; ?></td>
    <td><a href="/processwire/page/edit/?id=<?php echo $p->id; ?>#Inputfield_categories" rel="nofollow"><?php echo $p->title; ?></a></td>
    <td><?php echo $p->category1; ?></td>
    <td><?php echo $p->category2; ?></td>
    <td><?php echo $p->category3; ?></td>
    <td><?php echo $p->price; ?></td>
</tr>
<?php endforeach; ?>
</table>