<div class="nav nav-main">
<ul>
<!--li><a href="<?php echo $page->parent->url; ?>"><i class="fa fa-fw fa-chevron-left"></i> <?php echo $page->parent->title; ?></a></li>
<li class="current"><a href="<?php echo $page->parent->url; ?>"><?php echo $page->title; ?></a></li-->
<?php foreach($page->children as $c1): ?>
<li class="level1<?php if($page->id == $c1->id) {echo " current"; }; ?>">
<a href="<?php echo $c1->httpUrl; ?>"><?php echo $c1->title; ?></a></li>
<?php endforeach; ?>
</ul>
</div>