<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">


<link rel="shortcut icon" href="/site/templates/images/favicon.ico">
<link rel="icon" type="image/png" href="/site/templates/images/favicon.png" sizes="64x64">


<title><?php echo $title; // aus der _init.php ?></title>
<meta name="robots" content="noindex,nofollow">
<meta name="description" content="">
<meta property="og:image" content="">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel="stylesheet" href="/site/templates/styles/nomad.css">
</head>