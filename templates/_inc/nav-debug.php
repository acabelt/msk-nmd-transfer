<div class="n av n av-main cf">
<ul>
<?php foreach($ebene1 as $e1): ?>
<li class="level1<?php if($page->id == $e1->id) {echo " current"; }; ?>" style="float:left; width:200px;">
<a href="<?php echo $e1->httpUrl; ?>"><?php echo $e1->title; ?></a>
<?php if($e1.children.count>0): ?>
<ul>
    <?php foreach($e1->children as $c1): ?>
    <li class="level2<?php if($page->id == $c1->id) {echo " current"; }; ?>"><a href="<?php echo $c1->url; ?>"><?php echo $c1->title; ?></a>
    <?php if($c1.children.count>0): ?>
    <ul>
        <?php foreach($c1->children as $c2): ?>
        <li class="level3<?php if($page->id == $c2->id) {echo " current"; }; ?>"><a href="<?php echo $c2->url; ?>"><?php echo $c2->title; ?></a></li>
        <?php endforeach; ?>
    </ul>
    <?php endif; ?>
    </li>
    <?php endforeach; ?>
</ul>
<?php endif; ?>
</li>
<?php endforeach; ?>
</ul>
</div>