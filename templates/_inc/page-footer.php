<p>
    <a href="https://nmd.mskdev.com/mietkalkulator/">&copy; 2016</a>
    &ndash; Template: <code><?php echo $page->template; ?></code>
    &ndash; Datei: <code><?php echo $page->template; ?>.php</code>
    &ndash; Page ID: <code><?php echo $page->id; ?></code>
    &ndash; URL: <code><?php echo $page->httpUrl; ?></code>
</p>


<style>

    code {background:#ddd; color:#c00; padding:2px 3px; border-radius:3px; }
    li.current a {background:#ddd !important; color:#333 !important}
    
    .breadcrumb {padding:10px 30px; border-bottom:1px solid #333; margin:0;}
    .breadcrumb li {margin:0; padding:0; display:inline;}
    .breadcrumb li + li {margin-left:15px;}
</style>