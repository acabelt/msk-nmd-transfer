
<hr>
<ul>
<?php foreach($ebene1 as $e1): ?>
<li>
<?php echo $e1->title; ?> (ID: <?php echo $e1->id; ?> / Parent ID: <?php echo $e1->parent->id; ?> / URL: <?php echo $e1->httpUrl; ?>)
<?php if($e1.children.count>0): ?>
<ul>
    <?php foreach($e1->children as $c1): ?>
    <li><?php echo $c1->title; ?></li>
    <?php endforeach; ?>
</ul>
<?php endif; ?>
</li>
<?php endforeach; ?>
</ul>