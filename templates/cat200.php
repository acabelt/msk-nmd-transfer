<?php include_once("./_inc/html-head.php");  ?>
<body>
<div class="page">

<?php include_once("./_inc/page-header.php");  ?>
<?php include_once("./_inc/breadcrumb.php");  ?>


<div class="app">
    <?php include_once("./_inc/nav-100.php");  ?>
    <div class="products">
        <?php foreach($pages->find("template=product,category2=$page->id") as $p) { echo $p->render('views/view-product-item.php'); }; ?>
    </div>
</div><!-- /.app -->


<?php include_once("./_inc/page-footer.php");  ?>
</div><!-- /.page -->