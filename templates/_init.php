<?php namespace ProcessWire;

/**
 * Initialize variables output in _main.php
 *
 * Values populated to these may be changed as desired by each template file.
 * You can setup as many such variables as you'd like. 
 *
 * This file is automatically prepended to all template files as a result of:
 * $config->prependTemplateFile = '_init.php'; in /site/config.php. 
 *
 * If you want to disable this automatic inclusion for any given template, 
 * go in your admin to Setup > Templates > [some-template] and click on the 
 * "Files" tab. Check the box to "Disable automatic prepend file". 
 *
 */

// Variables for regions we will populate in _main.php
// Here we also assign default values for each of them.
$title = $page->get('headline|title'); 
$content = $page->body;
$sidebar = $page->sidebar;

// We refer to our homepage a few times in our site, so 
// we preload a copy here in $homepage for convenience. 
$homepage = $pages->get('/');

$ebene1 = $pages->find('menulevel=1');
$ebene2 = $pages->find('menulevel=2');
$ebene3 = $pages->find('menulevel=3');




// Produktabfragen
$produkte_alle = $pages->findMany('template=product');



$produktauswahl = $pages->findMany('template=product, limit=10, sort=id');




$brands = $pages->findMany('template=brand, sort=title');

//
$app = $page->get('template=app');






$lang = $user->language->name; // e.g. "default" or "en"

if($lang == 'default') {
	$htmlLang = 'de';
	$homeURL = $config->urls->root;
	$addtocart = "Zur Liste hinzufügen";
}
if($lang == 'en') {
	$htmlLang = $lang;
	$homeURL = $config->urls->root . 'en/';
	$addtocart = "Add to list";
}