<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">


<link rel="shortcut icon" href="/favicon.ico">
<link rel="icon" type="image/png" href="/favicon.png" sizes="64x64">


<title><?php echo $title; // aus der _init.php ?></title>
<meta name="robots" content="noindex,nofollow">
<meta name="description" content="">
<meta property="og:image" content="">
<style>
    body {font-family:Verdana, sans-serif;}
    code {background:#ddd; border-radius:3px; color:#c00; padding:2px 3px;}
</style>
</head>
<body class=" ">
<h1>Beispiele definiert in <code>catalogue.php</code></h1>
<p>Eine Produktauswahl basierend auf <code>$produktauswahl</code> definiert in <code>_init.php</code>. Die Ausgabe erfolgt als <code>array()</code>, wenn nicht anders definiert. Siehe Beispiel darunter.</p>
<?php echo $produktauswahl; ?>
<hr>

<p>Die Produktauswahl wie oben in einem <code>foreach()</code> ausgegeben inkl. HTML für Ausgabe.</p>
<table border="1">
<tr>
    <th>Name</th>
    <th>Preis</th>
    <th>Marke</th>
    <th>ID</th>
    <th>Bild</th>
</tr>
<!--?php foreach($produkte_alle->find('limit=10, vendor=2028') as $c): ?-->
<?php foreach($produktauswahl as $c): ?>
<tr>
<td><?php echo $c->title; ?></td>
<td><?php echo number_format($c->price, 2, ',', ' '); ?></td>
<td><a href="<?php echo $c->vendor->url; ?>"><?php echo $c->vendor->title; ?></a></td>
<td><?php echo $c->id; ?></td>
<td><?php if($c->images->first->url) { echo $c->images->first->url; } else { echo "/path/to/placeholder.png"; }; ?></td>
</tr>
<?php endforeach; ?>
</table>
<hr>
<p>Die Produktauswahl wie oben, auch in einem <code>foreach()</code>, aber mit einer <code>render('views/beispiel-ausgabe-produkte.php')</code> Anweisung und externem Template.</p>
<?php foreach($produktauswahl as $c) { echo $c->render('views/beispiel-ausgabe-produkte.php'); }; ?>