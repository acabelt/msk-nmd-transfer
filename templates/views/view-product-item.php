<!--
// übergeben wir mittels render() an eine externe "Steuerungsdatei" oder "Darstellungsanweisung (Template)",
// arbeiten wir hier wieder im Seiten ($page) Kontext und beziehen uns nicht mehr auf eventuell vergebene Variablen wie $c im foreach()-Aufruf
-->
<div class="item" style="background:#fff; margin:15px;">
    <span style="display:block;"><a href="/processwire/page/edit/?id=<?php echo $page->id; ?>" target="_blank" rel="nofollow"><?php echo $page->title; ?></a></span>
    <span style="display:block;">Preis: <?php echo $page->price; ?></span>
    <span style="display:block;">EAN: <?php echo $page->ean; ?></span>
    <span style="display:block;">Beschreibung: <?php echo $page->descriptionshort; ?></span>
    <span style="display:block;">
        <a href="#" onclick="alert('<?php echo $page->title; ?> (ID:<?php echo $page->id ?>) in den Warenkorb')" class="button"><?php echo $addtocart; ?></a>
    </span>
</div>