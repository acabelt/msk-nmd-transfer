<!--
// übergeben wir mittels render() an eine externe "Steuerungsdatei" oder "Darstellungsanweisung (Template)",
// arbeiten wir hier wieder im Seiten ($page) Kontext und beziehen uns nicht mehr auf eventuell vergebene Variablen wie $c im foreach()-Aufruf
-->
<div class="product" style="border:1px solid #ddd; margin:0 30px 30px 0; padding:10px; max-width:33%; min-height:120px; float:left;">
    <span style="display:block;"><strong><?php echo $page->title; ?></strong></span>
    <span style="display:block;">Preis: <?php echo $page->price; ?></span>
    <span style="display:block;">EAN: <?php echo $page->ean; ?></span>
    <span style="display:block;">Beschreibung: <?php echo $page->descriptionshort; ?></span>
</div>